from django.shortcuts import render
from todos.models import TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
